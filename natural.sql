-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 01:57 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `natural`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pollanswers`
--

CREATE TABLE `tbl_pollanswers` (
  `id` int(11) NOT NULL,
  `poll_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `poll_answer` varchar(10) NOT NULL,
  `eventname` varchar(50) NOT NULL,
  `poll_at` datetime NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pollanswers`
--

INSERT INTO `tbl_pollanswers` (`id`, `poll_id`, `users_id`, `poll_answer`, `eventname`, `poll_at`, `points`) VALUES
(3, 6, 565, 'opt1', 'chalhoub', '2020-12-14 08:59:11', 0),
(4, 7, 565, 'opt1', 'chalhoub', '2020-12-14 09:00:33', 1),
(5, 6, 566, 'opt2', 'chalhoub', '2020-12-17 14:28:15', 0),
(6, 6, 568, 'opt1', 'chalhoub', '2020-12-21 11:19:26', 0),
(7, 8, 568, 'opt2', 'chalhoub', '2020-12-21 11:20:59', 1),
(8, 6, 569, 'opt2', 'chalhoub', '2020-12-21 16:03:29', 0),
(9, 6, 570, 'opt1', 'chalhoub', '2020-12-29 13:42:13', 0),
(10, 6, 571, 'opt2', 'chalhoub', '2020-12-30 13:20:33', 0),
(11, 7, 575, 'opt4', 'chalhoub', '2021-03-05 16:32:06', 0),
(12, 7, 580, 'opt3', 'chalhoub', '2021-06-01 10:24:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_polls`
--

CREATE TABLE `tbl_polls` (
  `id` int(11) NOT NULL,
  `poll_question` varchar(500) NOT NULL,
  `poll_opt1` varchar(500) NOT NULL,
  `poll_opt2` varchar(500) NOT NULL,
  `poll_opt3` varchar(500) NOT NULL,
  `poll_opt4` varchar(500) NOT NULL,
  `correct_ans` varchar(10) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `poll_over` int(11) NOT NULL DEFAULT '0',
  `show_results` int(11) NOT NULL DEFAULT '0',
  `eventname` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_polls`
--

INSERT INTO `tbl_polls` (`id`, `poll_question`, `poll_opt1`, `poll_opt2`, `poll_opt3`, `poll_opt4`, `correct_ans`, `active`, `poll_over`, `show_results`, `eventname`) VALUES
(9, 'Whats the temp today', '25', '30', '35', '28', 'opt1', 0, 0, 0, 'chalhoub'),
(6, 'Whats the temperature today', '22', '21', '16', '35', 'opt3', 0, 0, 0, 'chalhoub'),
(7, 'Which country has the maximum number of awards', 'UAE', 'India', 'USA', 'France', 'opt1', 1, 0, 0, 'chalhoub'),
(8, 'When is the Christmas party', '9pm', '10pm', '11pm', '5pm', 'opt2', 0, 0, 0, 'chalhoub');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_empid` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0',
  `user_location` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_empid`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`, `user_location`) VALUES
(1, 'priyanka', 'pri@coact.co.in', 'abcd', '2022-01-13 11:32:05', 'natural', 0, 0, '-');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_empid` varchar(500) NOT NULL,
  `country` varchar(200) DEFAULT NULL,
  `checkbox` varchar(100) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_empid`, `country`, `checkbox`, `login_date`, `logout_date`, `logout_status`) VALUES
(1, 'priyanka', 'pri@coact.co.in', NULL, NULL, '2022-01-13 13:55:42', '2022-01-13 13:56:12', 1),
(2, 'priyanka', 'pri@coact.co.in', NULL, NULL, '2022-01-13 13:55:42', '2022-01-13 13:56:12', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pollanswers`
--
ALTER TABLE `tbl_pollanswers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_polls`
--
ALTER TABLE `tbl_polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
