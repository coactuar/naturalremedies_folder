<?php
	require_once "config.php";
	
	 if(!isset($_SESSION["user_empid"]))
	{
		header("location: index.php");
	 	exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
    
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where user_empid='".$_SESSION["user_empid"]."'";
           $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_empid"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Natural Remedies</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body class="bg1">

<!-- <div class="container-fluid">
    <div class="row">
    <img src="img/lptop-video.jpg" class="img-fluid desktopon" alt=""/> 
	 <img src="img/lptop-mobile.jpg" class="img-fluid mobileon" alt=""/> 
    </div>
/</div>     -->
<!-- <div class="container">    
    <div class="row">
        <div class="col-12 text-right">
             Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn btn-sm p-1 btn-danger">Logout</a> 
        </div>
    </div>  -->  
        <!-- <div class="row mt-1">
			<div class="col-12 col-md-1 desktopon">
			</div>
          <div class="col-12 col-md-10">
             <div class="embed-responsive embed-responsive-16by9 video-area">
                  <iframe class="embed-responsive-item video" src="video.php" allowfullscreen scrolling="no"></iframe>
              </div>
          </div>
		</div> 
		<div class="row mt-2 mb-2">
			<div class="col-12 col-md-1 desktopon">
			</div>
			<div class="col-12 col-md-10">
               <div id="polls" style="display:none;">
                  <div class="row mt-2">
                      <div class="col-12">
                          <iframe id="poll-question" src="#" width="100%" height="350" frameborder="0" scrolling="no"></iframe>
                      </div>
                  </div>    
              </div>
			
			  <form id="question-form" method="post" role="form" class="form-inline">
					<div class="form-group col-10 p-0">
                        
                        <input class="form-control" type="text" id="userQuestion" name="userQuestion" placeholder="Ask your Question" required >
                        
					</div>
					<div class="form-group col-2 p-0">
						<input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
						<input type="hidden" id="user_empid" name="user_empid" value="<?php echo $_SESSION['user_empid']; ?>">
						<input type="submit" class="submit"  alt="Submit" value="Submit">
					</div>
			  </form>
			  
			</div>
      </div> -->
    
    

 <!-- </div> -->



 <nav class="navbar m-0 p-0 bg1 nattop">
        
        <img src="images/logo12.jpg"  class="logoweb" width="110px" alt="">
        <img src="images/welcome-removebg-preview.png" width="200px" class="welweb" alt="">
        <img src="images/gptwlogo.jpg" class="float-right gpweb" width="110px" alt=""> 
</nav>



<div class="container-fluid">
  <div class="">
  <!-- <div class="row">
        
    </div>  -->
      <div class="row bg-grey">
            <div class="col-12 text-right">
                Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn btn-sm p-1 btn-danger">Logout</a> 
            </div>
            <div class="col-12 col-md-8 col-sm-6 text-center">
                <div class="embed-responsive">
                <div class="embed-responsive embed-responsive-16by9 video-area">
                  <iframe class="embed-responsive-item video" src="video.php" allowfullscreen scrolling="no"></iframe>
                </div>
                
                </div>
          </div>
            <div class="col-12 col-md-4 col-sm-6">
                <div class="login">
                <div id="polls" style="display:none;">
                  <div class="row mt-2">
                      <div class="col-12">
                          <iframe id="poll-question" src="#" width="100%" height="350" frameborder="0" scrolling="no"></iframe>
                      </div>
                  </div>    
              </div>
                    <div class="login-form">
                    <form id="question-form" method="post" role="form">
                          <div class="row">
                            <div class="col-10 commentweb">
                                <h6 class="text-dark bg-primary cmt">Comments</h6>
                                <div class="form-group">
                                   <textarea class="form-control" name="userQuestion" id="userQuestion" rows="140" cols="70" required></textarea>
                                </div>
                            </div>
                          </div>
                          <div class="row mt-3">
                            <div class="col-10">
                            <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
						<input type="hidden" id="user_empid" name="user_empid" value="<?php echo $_SESSION['user_empid']; ?>">
						<input type="submit" class="submit"  alt="Submit" value="Submit">
                                <div id="message"></div>
                            </div>
                          </div>  
                    </form>
                    </div>
                </div>
                <!-- <a href="?action=logout" class="btn btn-sm btn-danger">Logout</a> -->
            </div>
        </div>
    </div>
</div>



<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){

	$(document).on('submit', '#question-form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#message').removeClass('alert-danger fail');
                  $('#message').addClass('alert-success success'); 
                  $('#message').text('Your question is submitted successfully.').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#message').addClass('alert-danger fail');
                  $('#message').removeClass('alert-success success'); 
                  $('#message').text(data);
                }
                
            });
        
      
      return false;
    });
});


</script>
<script>
function pingPing(){
  var datauser = '<?= $_SESSION["user_empid"] ?>';
    $.ajax(
    {
        type:'POST',
        url:'logouttime.php', 
        data:{ empid: datauser },
        success: function(data) 
        {
            console.log(data);
            setTimeout(pingPing, 10000);
        }
    });

}
$( document ).ready(function() {
  pingPing();
});

var pollTimer;
function chkPolls()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'chkpoll'},
         type: 'post',
         success: function(output) {
			   if(output != 0)
			   {    
                    if($('#poll-question').attr('src') != output)
                   {
                       $("#poll-question").attr("src", output);
                       $('#polls').css('display','');
                   }
			   }
               else{
                   $("#poll-question").attr("src", '#');
                   $('#polls').css('display','none');
               }
         }
    });
}
chkPolls();
pollTimer = setInterval(function(){ chkPolls(); }, 3000);


</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-16"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-16');
</script>

</body>
</html>